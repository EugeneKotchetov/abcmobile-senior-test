<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\RegisterUser;
use App\Models\User;
use App\Notifications\RegisteredUserNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    /**
     * Register new user
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(RegisterUser $request): JsonResponse
    {
        $user =  User::create([
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);

        // обрабляем в try на случай, если не заполнены параметры для отправки почты в .env
        try {
            $user->notify(new RegisteredUserNotification());
        } catch (\Exception $e) {
            Log::error($e);
        }

        return response()->json([
            'message' => 'You were successfully registered.'
        ], 201);
    }

}
