<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UpdateUser;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /** @var UserService */
    private $userService;

    /**
     * Create a new command instance.
     *
     * @param UserService $userService
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * View auth user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(): JsonResponse
    {
        $user = Auth::guard('api')->user();

        return response()->json($user->toArray(), 200);
    }

    /**
     * Update auth user
     *
     * @param UpdateUser $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(UpdateUser $request): JsonResponse
    {
        $user = Auth::guard('api')->user();
        $this->userService->update($user, $request->all());

        return response()->json('User update was successful.', 200);
    }

}
