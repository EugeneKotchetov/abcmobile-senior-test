<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    /**
     * Update user
     *
     * @param User $user
     * @param array $data
     * @return void
     */
    public function update(User $user, array $data): void
    {
        $user->language = $data['language'];
        $user->timezone = $data['timezone'];
        $user->save();
    }
}
