<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateUserTest()
    {
        $user = factory(User::class)->create();

        $userService = resolve(UserService::class);
        $userService->update($user, [
            'language' => 'language_updated',
            'timezone' => 'timezone_updated'
        ]);

        $this->assertEquals($user->language, 'language_updated');
        $this->assertEquals($user->timezone, 'timezone_updated');
    }
}
