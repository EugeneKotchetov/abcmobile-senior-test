<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Register user test.
     *
     * @return void
     */
    public function testRegisterUser()
    {
        $faker = Factory::create();

        $response = $this->post('/api/auth/register', [
            'email' => $faker->unique()->safeEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertStatus(201);
    }

    /**
     * Login user test.
     *
     * @return void
     */
    public function testLoginUser()
    {
        /**
         *
         * Without Artisan call you will get a passport
         * "please create a personal access client" error
         */
        \Artisan::call('passport:install');

        $user = factory(User::class)->create();

        $response = $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'token' => true,
            ]);

        $this->assertAuthenticatedAs($user, $guard = null);
    }

    /**
     * Update user test.
     *
     * @return void
     */
    public function testUpdateUser()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $response = $this->put('/api/user', [
            'language' => 'Russian',
            'timezone' => 'RU'
        ]);

        $response->assertStatus(200);
    }

    /**
     * View user test.
     *
     * @return void
     */
    public function testViewUser()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $response = $this->get('/api/user');

        $response->assertStatus(200)
            ->assertJson([
                'name' => $user->name,
                'email' => $user->email,
                'language' => $user->language,
                'timezone' => $user->timezone
            ]);

    }
}
