# ABC-mobile senior test
[Тестовое_для_senior.pdf](Тестовое_для_senior.pdf)

## Local start up

1. Ensure you have .env file 

2. Build docker container
`docker-compose build`

3. Start up docker
`docker-compose up -d`
 
4. Install composer
`docker-compose exec app composer install`

5. Run migrations
`docker-compose exec app php artisan migrate`

6. Generate Passport personal access client
`docker-compose exec app php artisan passport:install`

7. Link public dir 
`docker-compose exec app php artisan storage:link`

8. Testing 
`docker-compose exec app php artisan test`

## Postman collection

[AbcMobile.postman_collection.json](AbcMobile.postman_collection.json)

## 
